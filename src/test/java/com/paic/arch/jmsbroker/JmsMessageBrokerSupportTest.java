package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.enums.MqType;
import com.paic.arch.jmsbroker.exception.MqException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;

    @BeforeClass
    public static void setup() throws Exception {
        JmsMessageBrokerSupportTemplate.runningEmbeddedBrokerOnAvailablePort();
        JMS_SUPPORT =   JmsMessageFactory.createJmsMessageBrokerSupport(JmsMessageBrokerSupportTemplate.getBrokerUrl(), MqType.ActiveMq);;
    }

    @AfterClass
    public static void teardown() throws Exception {
        JmsMessageBrokerSupportTemplate.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        JMS_SUPPORT
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JmsMessageBrokerSupportTemplate.getJmsMessageService().getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JMS_SUPPORT
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = MqException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        JMS_SUPPORT.retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }


}
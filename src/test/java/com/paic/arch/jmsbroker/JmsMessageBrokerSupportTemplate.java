package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMqService;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

/**
 * @author Benny
 * @version V1.0
 * @Description:   *
 * @Date 2018/4/12
 */
public class JmsMessageBrokerSupportTemplate {
    public static String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
    private static JmsMessageService jmsMessageService;

    public static void runningEmbeddedBrokerOnAvailablePort() throws Exception {
        DEFAULT_BROKER_URL_PREFIX = DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000);
        jmsMessageService = new ActiveMqService(DEFAULT_BROKER_URL_PREFIX);
        jmsMessageService.start();

    }

    public static String getBrokerUrl() {
        return DEFAULT_BROKER_URL_PREFIX;
    }

    public static void stopTheRunningBroker() {
        try {
            jmsMessageService.stop();
        } catch (Exception e) {
        }
    }

    public static JmsMessageService getJmsMessageService() {
        return jmsMessageService;
    }
}
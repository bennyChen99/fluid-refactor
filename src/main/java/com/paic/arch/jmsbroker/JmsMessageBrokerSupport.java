package com.paic.arch.jmsbroker;

import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description:   Mq 消费、生成 提供器
 * 1、使用接口代替具体mq代理，对具体实现进行隔离，使得JmsMessageBrokerSupport更加通用，职责更加单一，
 * 2、使用接口进行隔离，使得对外职责更加清晰
 * @Date 2018/4/12
 */
public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    // public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private JmsMessageReceiver jmsMessageReceiver;
    private JmsMessageSender jmsMessageSender;

    private JmsMessageBrokerSupport(JmsMessageReceiver jmsMessageReceiver, JmsMessageSender jmsMessageSender) {
        this.jmsMessageReceiver = jmsMessageReceiver;
        this.jmsMessageSender = jmsMessageSender;
    }

    public static JmsMessageBrokerSupport newInstance(JmsMessageReceiver jmsMessageReceiver, JmsMessageSender jmsMessageSender) throws Exception {
        return new JmsMessageBrokerSupport(  jmsMessageReceiver,  jmsMessageSender);
    }


    public final JmsMessageBrokerSupport andThen() {
        return this;
    }


    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        jmsMessageSender.sendATextMessageToDestinationAt(aDestinationName,aMessageToSend);
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return  jmsMessageReceiver.retrieveASingleMessageFromTheDestination(aDestinationName,aTimeout);
    }





}
package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.JmsMessageSender;
import org.slf4j.Logger;

import javax.jms.MessageProducer;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description: ActiveMq 生成者*
 * @Date 2018/4/12
 */
public class ActiveMqSender extends ActiveMqBase implements JmsMessageSender {
    private static final Logger LOG = getLogger(ActiveMqSender.class);
    private String brokerUrl;

    public ActiveMqSender(String brokerUr) {
        this.brokerUrl = brokerUr;
    }

    public static ActiveMqSender newInstance(String brokerUrl) {
        ActiveMqSender ret = new ActiveMqSender(brokerUrl);
        return ret;
    }

    @Override
    public String sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            LOG.info(String.format("producer: msg [%s] send out to [%s]", aMessageToSend, aDestinationName));
            producer.close();
            return "";
        });
    }

}
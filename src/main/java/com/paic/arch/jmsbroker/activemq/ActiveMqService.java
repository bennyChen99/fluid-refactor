package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.JmsMessageService;
import com.paic.arch.jmsbroker.exception.MqException;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description:   ActiveMq 服务器*
 * @Date 2018/4/12
 */
public class ActiveMqService implements JmsMessageService {
    private static final Logger LOG = getLogger(ActiveMqService.class);
    private BrokerService brokerService;
    private String  brokerUrl;
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

    public ActiveMqService(String brokerUrl) throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
        this.brokerUrl = brokerUrl;
    }
    @Override
    public void start() throws Exception {
        brokerService.start();
    }

    @Override
    public void stop() throws Exception{
        if (brokerService == null) {
            throw new MqException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }


    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

    @Override
    public DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new MqException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    @Override
    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }


}
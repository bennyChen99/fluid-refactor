package com.paic.arch.jmsbroker.activemq;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * @author Benny
 * @version V1.0
 * @Description:   *
 * @Date 2018/4/12
 */
public interface JmsCallback {
    String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
}
package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.exception.MqException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;

import javax.jms.*;

import static com.paic.arch.jmsbroker.exception.DaoExceptionCodes.ZA030002;
import static com.paic.arch.jmsbroker.exception.DaoExceptionCodes.ZA030003;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description: ActiveMq基类*
 * @Date 2018/4/12
 */
public class ActiveMqBase {

    private static final Logger LOG = getLogger(ActiveMqBase.class);

    protected String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
        // ConnectionFactory ：连接工厂，JMS 用它创建连接
        ConnectionFactory connectionFactory;
        Connection connection = null;

        String returnValue = "";

        try {
            connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            //构造从工厂得到连接对象
            connection = connectionFactory.createConnection();
            //启动
            connection.start();

            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);

        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new MqException(ZA030002);
        } finally {
            if (null != connection) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new MqException(ZA030003);
                }
            }
        }
        return returnValue;
    }


    protected String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        // Session： 一个发送或接收消息的线程
        Session session = null;
        try {
            session = aConnection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            Queue destination = session.createQueue(aDestinationName);

            return aCallback.performJmsFunction(session, destination);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
}
package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.JmsMessageReceiver;
import com.paic.arch.jmsbroker.exception.MqException;
import org.slf4j.Logger;

import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import static com.paic.arch.jmsbroker.exception.DaoExceptionCodes.ZA032001;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description:   ActiveMq 消费者*
 * @Date 2018/4/12
 */
public class ActiveMqReceiver  extends ActiveMqBase implements JmsMessageReceiver {
    private static final Logger LOG = getLogger(ActiveMqReceiver.class);

    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    private String brokerUrl;
    public ActiveMqReceiver(String brokerUr){
        this.brokerUrl = brokerUr;
    }

    public static ActiveMqReceiver newInstance(String brokerUrl){
        ActiveMqReceiver ret = new ActiveMqReceiver(brokerUrl);
        return ret;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (null == message) {
                throw new MqException(ZA032001);
            }
            consumer.close();
            LOG.info(String.format("consumer: msg [%s] received from [%s]", ((TextMessage) message).getText(), aDestinationName));
            return ((TextMessage) message).getText();
        });
    }
}
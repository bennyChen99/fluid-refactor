package com.paic.arch.jmsbroker.exception;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义Mq异常类 异常代码列表  *
 * @Date 2018/4/11
 */

public class DaoExceptionCodes {
    public static final String ZA030000 = "MQ异常!--[ZA030000]";
    public static final String ZA030001 = "初始化MQ类型异常!--[ZA030001]";
    public static final String ZA030002 = "初始化MQ异常!--[ZA030002]";
    public static final String ZA030003 = "关闭MQ连接出错!--[ZA030003]";


    public static final String ZA031001 = "MQ[producer]异常!--[ZA031001]";

    public static final String ZA032001 = "MQ[consumer]超时未接收到任何消息!--[ZA032001]";


}

package com.paic.arch.jmsbroker.exception;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义MQ异常类  *
 * @Date 2018/4/11
 */

public class MqException extends RuntimeException {
    public MqException(String reason) {
        super(reason);
    }
}
package com.paic.arch.jmsbroker;

/**
 * @author Benny
 * @version V1.0
 * @Description:   Mq 消费者 接口类*
 * @Date 2018/4/12
 */
public interface JmsMessageReceiver {
    String retrieveASingleMessageFromTheDestination(String aDestinationName);

    String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout);
}
package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.region.DestinationStatistics;

/**
 * @author Benny
 * @version V1.0
 * @Description:   Mq 服务器 接口类*
 * @Date 2018/4/12
 */
public interface JmsMessageService {

    void start() throws Exception;
    void stop() throws Exception;



    long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

    DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception;

    boolean isEmptyQueueAt(String aDestinationName) throws Exception;
}
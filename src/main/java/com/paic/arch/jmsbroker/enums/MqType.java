package com.paic.arch.jmsbroker.enums;

/**
 * @author Benny
 * @version V1.0
 * @Description: MQ类型列表  *
 * @Date 2018/4/11
 */
public enum MqType {
    ActiveMq,
    IbmMq,
    TibcoMq;
}
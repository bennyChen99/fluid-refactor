package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMqReceiver;
import com.paic.arch.jmsbroker.activemq.ActiveMqSender;
import com.paic.arch.jmsbroker.enums.MqType;
import com.paic.arch.jmsbroker.exception.MqException;
import org.slf4j.Logger;

import static com.paic.arch.jmsbroker.exception.DaoExceptionCodes.ZA030001;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Benny
 * @version V1.0
 * @Description:   Mq 工厂
 * 1、使用接口代替具体mq代理，对具体实现进行隔离，使得JmsMessageBrokerSupport更加通用，职责更加单一，
 * 2、使用接口进行隔离，使得对外职责更加清晰
 * @Date 2018/4/12
 */
public class JmsMessageFactory {

    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);

    public static JmsMessageBrokerSupport createJmsMessageBrokerSupport(String brokerUrl, MqType type) throws Exception {
        LOG.debug("Creating a new broker at {}", brokerUrl);
        JmsMessageReceiver jmsMessageReceiver= null ;
        JmsMessageSender jmsMessageSender = null;
        switch (type) {
            case ActiveMq:
                jmsMessageReceiver = ActiveMqReceiver.newInstance(brokerUrl);
                jmsMessageSender = ActiveMqSender.newInstance(brokerUrl);
                break;
            default:
                throw new MqException(ZA030001);
        }
        JmsMessageBrokerSupport broker = JmsMessageBrokerSupport.newInstance(jmsMessageReceiver,jmsMessageSender);
        return broker;
    }

}
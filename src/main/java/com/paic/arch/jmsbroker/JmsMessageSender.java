package com.paic.arch.jmsbroker;

/**
 * @author Benny
 * @version V1.0
 * @Description:   Mq 生产者 接口类*
 * @Date 2018/4/12
 */
public interface JmsMessageSender {
    String sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend);
}